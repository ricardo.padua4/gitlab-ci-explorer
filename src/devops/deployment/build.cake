﻿// Install addins.
#addin "nuget:https://api.nuget.org/v3/index.json?package=Cake.Coveralls&version=1.0.0"
#addin "nuget:https://api.nuget.org/v3/index.json?package=Cake.Npm&version=1.0.0"
#addin "nuget:https://api.nuget.org/v3/index.json?package=Cake.Flutter&version=1.0.0"
#addin "nuget:https://api.nuget.org/v3/index.json?package=Newtonsoft.Json&version=13.0.1"

// Install tools.
#tool "nuget:https://api.nuget.org/v3/index.json?package=coveralls.io&version=1.4.2"
#tool "nuget:https://api.nuget.org/v3/index.json?package=OpenCover&version=4.7.922"b  '
#tool "nuget:https://api.nuget.org/v3/index.json?package=ReportGenerator&version=4.7.1"
#tool "nuget:https://api.nuget.org/v3/index.json?package=NuGet.CommandLine&version=5.9.1"

// Load other scripts.
// #load "./utils.cake"
// #load "./Settings.cake"
// #load "./Tests.cake"


using System;
using System.Text.RegularExpressions;

var target = Argument("target", "Build");
var enviroment = Argument("enviroment", "playground");
var verbosity = Argument("verbosity", "diagnostic");
var skipNpm = Argument("skipNpm", false);

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// PARAMETERS PASSED AT SCRIPT CALL FOR CONFIGURATION FILES FOR DEPLOY IN CLOUD ENVIRONMENT
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

var PathNodeModules = "../../backend/WebApi/ClientApp/node_modules";
var PathClientApp = "../../backend/WebApi/ClientApp";
var pathWeb = "../../backend/WebApi";
var pathPublish = "bin/Release/netcoreapp3.1/publish";
var prologycDockerfile = Argument("prologycDockerfile", $"../environment/{enviroment}/webapi.{enviroment}");

// TASKS FOR GENERATING ARTIFACTS FOR CLIENT WEB AND DEPLOYMENT ARTIFACTS FOR CLIENT WEB IN CLOUD
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Task("ClearClientApp").WithCriteria(() => DirectoryExists($"{PathNodeModules}")).Does(() => DeleteDirectory($"{PathNodeModules}", DelDirSettings));
Task("ClearWeApi").WithCriteria(() => DirectoryExists($"{pathWeb}/bin/Release")).Does(() => DeleteDirectory($"{pathWeb}/bin/Release", DelDirSettings));
Task("NpmInstall").WithCriteria(() => !skipNpm).Does(() => NpmInstall(settings => settings.FromPath($"{PathClientApp}")));
Task("BuildClientApp").WithCriteria(() => !skipNpm).Does(() => NpmRunScript("build", settings => settings.FromPath($"{PathClientApp}")));
Task("PublishWebApi").Does(() => DotNetCorePublish($"{pathWeb}", DotNetSettingsPublishForWindows));
Task("CopyWebAPiDockerfile").Does(() => CopyFile(prologycDockerfile, $"{pathWeb}/{pathWebPublish}/manifest.yml"));
Task("PushWebApi").Does(() => Information("command for deploy artifact"));

Task("DeployWebArtifact")
    .IsDependentOn("ClearClientApp")
    .IsDependentOn("ClearWeApi")
    .IsDependentOn("NpmInstall")
    .IsDependentOn("BuildClientApp")
    .IsDependentOn("PublishWebApi")
    .Does(() => Information("TASK DEPLOY WEB ARTIFACT FINISHED"));

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// DEPLOY FOR TESTS IN PLAYGROUND OR SIT ENVIRONMENT
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Task("Deploy")
    .IsDependentOn("DeployWebArtifact")
    .Does(() => Information($"TASK DEPLOY ALL ARTIFACT FINISHED FOR ENVIRONMENT {enviroment} !"));

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// EXECUTION
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

RunTarget(target);